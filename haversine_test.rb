require 'minitest/autorun'
require File.join(File.dirname(__FILE__), "haversine")

describe Haversine do

  it "should return the distance between two points" do
    haversine = Haversine.new

    point1 = { latitude: 37.785970699, longitude: -122.435511723 }
    point2 = { latitude: 37.780425,    longitude: -122.4380895   }

    distance = haversine.distance_in_meters(point1[:latitude ],
                                            point1[:longitude],
                                            point2[:latitude ],
                                            point2[:longitude])

    distance.must_equal 656.9483951038181 # well-known value
  end 

end
